### I wanted to model a traditional search game

From [Wikipedia](https://en.wikipedia.org/wiki/Search_game),
>>>
A search game is a two-person zero-sum game which takes place in a set called the search space. The searcher can choose any continuous trajectory subject to a maximal velocity constraint. It is always assumed that neither the searcher nor the hider has any knowledge about the movement of the other player until their distance apart is less than or equal to the discovery radius and at this very moment capture occurs. As mathematical models, search games can be applied to areas such as hide-and-seek games that children play or representations of some tactical military situations. 
>>>

Here is the summary of my search game:
1. One Predator and one Prey initialize at opposite ends of some space.
2. Predator and Prey randomly walk. 
3. Predator and Prey may have different stepsize and steps per iteration.
4. When Predator sees Prey, Predator seeks Prey 
5. When Prey sees Predator, Prey evades Predator. 
6. For Predator to see Prey, Predator sight radius must be greater or equal to that of Prey.
5. For Predator to catch Prey, stepsize*steps per iteration of Predator must often be higher than that of Prey.

I modelled this behaviour first on a discrete grid and then in continuous space.

### Grid

There are two phases to a search game: wandering and running. 
Here, wandering is a random walk. For each iteration, each actor moves steps*stepsize in a new direction. 
If that direction would bring the actor outside of the search space, choose new direction. 
If an opposing actor is within sight radius, the actor runs according their movement rules; 
walking and running speeds were different originally, but this only slowed down the wandering phase.
The actor either moves directly towards or directly away from opposing actor (or orthogonoal to wall and opposing actor;
otherwise Prey will quickly become trapped against a wall or corner).
If actors are within interaction radius, Predator has caught Prey. Game over.

<!--
There are three ways Predator can seek and catch Prey:
1. Both randomly walk until contact.
2. Predator (with sight radius of ~infinity) immediately seeks Prey.
  * And Prey (with sight radius of ~infinity) immediately evades Predator.
  * Or Prey (with limited sight radius) evades Predator when Prey sees Predator
3. Both Predator and Prey randomly walk until sight.
-->

A search game will have some combination of sight radii:
<!--
1. Sight radius of 0 for Predator and/or Prey.
2. Sight radius of [1, inifinity) for Predator and/or Prey.
3. Sight radius of infinity for Predator and/or Prey.
-->
<!--
| | Predator |
| --- | --- |
| Prey | Table Below |
-->

| __Sight Radii__ | Predator 0 | Predator [1, infinity) | Predator infinity |
| --- | --- | --- | --- |
| Prey 0 | Predator and Prey randomly walk forever | Prey randomly walks forever; Predator randomly walks until sees Prey, then seeks Prey | Prey randomly walks forever; Predator immediately seeks Prey |
| Prey [1, infinity) | Predator randomly walks forever; Prey randomly walks until sees Predator, then evades Predator | Predator randomly walks until seeks Prey, then seeks Prey; Prey randomly walk until sees Predator, then evades Predator | Predator immediately seeks Prey; Prey randomly walks until sees Predator, then evades Predator |
| Prey infinity | Predator randomly walks forever; Prey immediately evades Predator | Predator randomly walks until sees Prey, then seeks Prey; Prey immediately evades Predator | Predator immediately seeks Prey; Prey immediately evades Predator |

The above scenarios will continue until contact or stopped. A few scenarios will now be shown. 
For all visualizations, the Prey (circle) began in the bottom-left corner and the Predator began in the top-right corner (triangle). 
The contact location is shwon as a larger shape.


With Predator and Prey stepsize=1 and steps equal, and for scenario (1,1) (both randomly walk) of the above table, 
the below search game was  was generated. 
Both Predator and Prey randomly walked for almost 3700 iterations until contact.
![RandomSearch](/Grid/RandomSearch.png)

With Predator stepsize=3 and Prey stepsize=1 and steps equal, and for scenario (1,3) (Prey randomly walks, Predator immediately seeks), the below search game was generated. 
The Predator contacted the Prey in less than 80 iterations.
![DirectSeekSearch](/Grid/DirectSeekSearch.png)

For the above two vizualizations, wandering AND running is shown as a dotted line. For the visualizations below, running is shown as a bold dashed line.

With Predator and Prey stepsize=1 and steps equal, and for scenario (1,2) (Prey randomly walks, Predator randomly walks until sees Prey), the below search game was generated. 
The sight radius of Predator was _sqrt_(_N_) where _N_ is the length of one side of the search space.
Predator contacted Prey in almost 1000 iterations.
![HorizonSeekSearch](/Grid/HorizonSeekSearch.png)


With Predator stepsize=8 and steps=1 (moves 8 distances 1 time), and Prey stepsize=5 and steps=2 (moves 5 distances 2 twices), 
and for scenario (2,2) (both randomly walk until sees other), the below search games were generated. 
My idea regarding stepsize and steps was that Predator was quicker but Prey was more agile. 
A fine balance between stepsize and steps is required to generate an interesting search game;
any values outside some codependent range makes Predator quickly contact Prey or Predator never contact Prey.
The sight radius of Predator was 2*_sqrt_(_N_), sight radius of Prey was _sqrt_(_N_). 
My idea regarding sight radius was that Predator would discover Prey first, getting the "jump" on Prey.
![HorizonSeekEvadeSearch](/Grid/HorizonSeekEvadeSearch.png)
![HorizonSeekEvadeSearch2](/Grid/HorizonSeekEvadeSearch2_1.png)
![HorizonSeekEvadeSearch2](/Grid/HorizonSeekEvadeSearch2_2.png)
![HorizonSeekEvadeSearch2](/Grid/HorizonSeekEvadeSearch2_3.png)

In these final four search games, Prey is chaotic and has wall detection during its evasion:
Chaotic here means that if one dimension is equal to that of Predator, Prey chooses randomly which direction to go 
(see first of these four);
and if Prey is near a wall, its new chosen direction is orthogonal to the wall and Predator 
(compared to solely antiparallel to Predator).

I has also planned to add: a "smell" radius where Predator and Prey would learn the general location of the other; 
and tiring effect; 
and stealth and other evasion maneuvers;
and landscape features that attracted or were impassable.
These were postponed during migration from discrete space to continuous space...

### Continuous
After playing with search games in discrete space with only nine possible directions, 
I decided to reimplement the above into a continuous search space.
With this second attempt I also employed OOP here (finally);
as always, see the code ([NondiscreteSeekEvade2.py](https://gitlab.com/Bonatt/SearchGame/blob/master/Continuous/NondiscreteSeekEvade2.py)) for more documentation.

It turns out that the mathematics involved are more complicated than adding a minus sign to the others x and/or y values. 
Wandering directions are chosen using trigonometric functions, and running directions are found via
<!-- ![RunMath](/Continuous/RunMath.png =513x480) 1026x960 -->
<img src="/Continuous/RunMath.png", width="513", height="480" />

Unfortunately I moved on to other projects before I could fully reimplement all features of the Grid version, 
namely wall inclusion during Prey's evasion phase. And on top of (because of) that, the old plot below is not all that great. 
Look at Prey get trapped in the corner.

![NondiscreteSeekEvade2](/Continuous/NondiscreteSeekEvade2.png)
