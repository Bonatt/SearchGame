import math
import random
import numpy as np
import ROOT


# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
def sin(x):
  return math.sin(x)
def cos(x):
  return math.cos(x)
# Redefine pi to something shorter
pi = math.pi
print ''


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(56); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y. 56:lo=y/hi=r. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();






### Two identities wander around room until the touch interact via some interaction radius.
# Let's do this again, except this time P2 seeks P1.
# Now with horizon range.
# Now with P1 evading P2.
# Now with wall detection.
# Try different levels of horizon, eg knowing something exists Southeast, but not exactly where;
#  Solid angle inversely proportional to distance apart.
#  Try to also redo direction vectors. Optimal vector away from P2 is orthogonal to both P2 and walls.
#  Use continuous vector analysis instead of discrete matrices.
# Add tiring effect.
# Add stealth, evading maneuvers (jump)
# Features on grid to attract or impassable


### Room dimensions
N = 10
height = N
width = N

### Identities and starting position (x,y)
P1 = [ (width/10., height/10.) ]
P2 = [ (width-width/10., height-height/10.) ]

### Number of steps a turn. Previously a multiplier, now n steps instead of stepsize n. Why not both?
steps1 = 2
stepsize1 = 0.1
steps2 = 2 
stepsize2 = 0.1

### Horizon (visible range, line-of-sight...) and interaction length.
H1 = sqrt(N)
H2 = sqrt(N)*1.5
L = stepsize2 #sqrt(N)/N



g1 = ROOT.TGraph()
g2 = ROOT.TGraph()
g1.SetPoint(g1.GetN(), P1[0][0], P1[0][1])
g2.SetPoint(g2.GetN(), P2[0][0], P2[0][1])

g1H = ROOT.TGraph() 
g2H = ROOT.TGraph()


# Precision digits for rounding...
prec = 3


### Boolean for out of bounds.
def OutOfBounds(x, y):
  # x = float, y = float
  if (x < 0) or (x > width) or (y < 0) or (y > height):
    return True
  else:
    return False
   


### Generate new wandering position given initial position.
def Wander(coords, stepsize):
  # coords = (x,y), stepsize = float
  xi = coords[0]
  yi = coords[1]
  r = stepsize
  theta = random.random()*2*pi
  xf = xi + r*cos(theta)
  yf = yi + r*sin(theta)

  # If any value goes out of bounds, bring it to border.
  if (xf < 0): xf = 0
  if (xf > width): xf = width
  if (yf < 0): yf = 0
  if (yf > height): yf = height
  #if OutOfBounds(xf,yf) == True:
    
  return (round(xf,prec), round(yf,prec))
  



# From https://math.stackexchange.com/questions/175896/finding-a-point-along-a-line-a-certain-distance-away-from-another-point
def Run(p1,p2,stepsize):
  # coords = (x,y), stepsize = float
  p1xi = p2[0]
  p1yi = p2[1]
  p2x = p1[0]
  p2y = p1[1]

  if p1yi-p2y == 0:
    d = sqrt( (p1xi-p2x)**2)
  elif p1xi-p2x == 0:
    d = sqrt( (p1yi-p2y)**2)
  else:
    d = sqrt( (p1xi-p2x)**2 + (p1yi-p2y)**2 )
  dt = stepsize

  t = dt/d

  return t



### Generate new chase position given initial p2 position and p1 position. Move closer by stepsize.
def Chase(p2,p1,stepsize):
  # coords = (x,y), stepsize = float
  p2xi = p2[0]
  p2yi = p2[1]
  p1x = p1[0]
  p1y = p1[1]
 
  t = Run(p2,p1,stepsize)

  p2xf = p2xi - t*p2xi + t*p1x
  p2yf = p2yi - t*p2yi + t*p1y

  return (round(p2xf,prec), round(p2yf,prec))




### Generate new flee postion given initial p1 position and p2 position. Move directly away by stepsize.
def Flee(p1,p2,stepsize):
  # coords = (x,y), stepsize = float
  p1xi = p1[0]
  p1yi = p1[1]
  p2x = p2[0]
  p2y = p2[1]

  t = -Run(p1,p2,stepsize)

  p1xf = p1xi - t*p1xi + t*p2x
  p1yf = p1yi - t*p1yi + t*p2y

  # If any value goes out of bounds, bring it to border.
  if (p1xf < 0): p1xf = 0
  if (p1xf > width): p1xf = width
  if (p1yf < 0): p1yf = 0
  if (p1yf > height): p1yf = height


  return (round(p1xf,prec), round(p1yf,prec))
  '''
  # Randomize movement in ~cone from P2
  r = sqrt( (p1xf-p1xi)**2 + (p1yf-p1yi)**2 )
  theta = np.arctan( (p1yf-p1yi)/(p1xf-p1xi) ) + random.randint(-90,90)*pi/180
  x = p1xi + r*cos(theta)
  y = p1yi + r*sin(theta)
  return (round(x,prec), round(y,prec))
  '''


### Euclidean distance, r = sqrt( (x-x0)**2 + (y-y0)**2 for 2D.
def EuclideanDistance(p1,p2):
  # p1 = (x1,y1), p2 = (x2,y2)
  distance = 0
  for i1,i2 in zip(p1,p2):
    distance += (i1 - i2)**2
  return sqrt(distance)


### Boolean for if interaction.
def Interaction(p1,p2):
  # p1 = (x1,y1), p2 = (x2,y2)
  distance = EuclideanDistance(p1,p2)
  if distance <= L:
    return True
  else:
    return False


### Boolean for if within horizon
def HasVision(p1,p2, h):
  distance = EuclideanDistance(p1,p2)
  if distance <= h:
    return True
  else:
    return False 










### Main loop
i = 0
while True:

  if HasVision(P1[-1], P2[-1], H1) == True:
    for steps in xrange(steps1):
      p1 = Flee(P1[-1], P2[-1], stepsize1)
      P1.append(p1)
      g1H.SetPoint(g1H.GetN(), p1[0], p1[1])
  else:
    p1 = Wander(P1[-1], stepsize1)
    P1.append(p1)
    g1.SetPoint(g1.GetN(), p1[0], p1[1])


  if HasVision(P2[-1], P1[-1], H2) == True:
    for steps in xrange(steps2):
      p2 = Chase(P2[-1], P1[-1], stepsize2)
      P2.append(p2)
      g2H.SetPoint(g2H.GetN(), p2[0], p2[1])
  else:
    p2 = Wander(P2[-1], stepsize2)
    P2.append(p2)
    g2.SetPoint(g2.GetN(), p2[0], p2[1])


  if Interaction(P1[-1],P2[-1]) == True: break

  i += 1
  if i == 10000: break

print 'Steps:', i







### Now reconstruct position.
lineWidth = 2
# From https://root.cern.ch/doc/master/classTStyle.html#a94c9ae34f921c1efaa09348c2d16d60a
ROOT.gStyle.SetLineStyleString(5, '4 16');
lineStyle = 5

markerSize = 2
p1markerStyle = ROOT.kOpenCircle
p2markerStyle = ROOT.kOpenTriangleUp

p1Color = ROOT.kOrange
p1HColor = ROOT.kRed
p2Color = ROOT.kViolet
p2HColor = ROOT.kBlue

c = ROOT.TCanvas('c', 'c', 720*width/height, 720)

g1.SetTitle('With Interaction Length '+str(round(L,prec))+' and Horizon '+str(round(H2,prec))+', Interaction in '+str(i)+' Steps;;')
g1.GetXaxis().SetLimits(0, width)
g1.GetYaxis().SetRangeUser(0, height)
g1.SetLineColor(p1Color)
g1.SetLineWidth(lineWidth)
g1.SetLineStyle(lineStyle)
g1.Draw('pal')

g2.SetLineColor(p2Color)
g2.SetLineWidth(lineWidth)
g2.SetLineStyle(lineStyle)
g2.Draw('pl same')

g1H.SetLineColor(p1HColor)
g1H.SetLineWidth(lineWidth)
g1H.SetLineStyle(ROOT.kDashed)
g1H.Draw('pl same')

g2H.SetLineColor(p2HColor)
g2H.SetLineWidth(lineWidth)
g2H.SetLineStyle(ROOT.kDashed)
g2H.Draw('pl same')


gInt = ROOT.TGraph(2, np.array((float(p1[0]), float(p2[0]))), np.array((float(p1[1]), float(p2[1]))))
gInt.SetLineColor(ROOT.kBlack)
gInt.Draw('l same')

gEnd1 = ROOT.TGraph(1, np.array(float(p1[0])), np.array(float(p1[1])))
gEnd1.SetMarkerStyle(p1markerStyle)
gEnd1.SetMarkerSize(markerSize)
gEnd1.SetMarkerColor(ROOT.kBlack)
gEnd1.Draw('p same')
gEnd2 = ROOT.TGraph(1, np.array(float(p2[0])), np.array(float(p2[1])))
gEnd2.SetMarkerStyle(p2markerStyle)
gEnd2.SetMarkerSize(markerSize)
gEnd2.SetMarkerColor(ROOT.kBlack)
gEnd2.Draw('p same')

gStart1 = ROOT.TGraph(1, np.array(float(P1[0][1])), np.array(float(P1[0][0])))
gStart1.SetMarkerStyle(p1markerStyle)
gStart1.SetMarkerColor(ROOT.kBlack)
gStart1.Draw('p same')
gStart2 = ROOT.TGraph(1, np.array(float(P2[0][1])), np.array(float(P2[0][0])))
gStart2.SetMarkerStyle(p2markerStyle)
gStart2.SetMarkerColor(ROOT.kBlack)
gStart2.Draw('p same')





c.SaveAs('NondiscreteSeekEvade2.png')

