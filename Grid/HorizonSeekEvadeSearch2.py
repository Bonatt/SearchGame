import math
import random
import numpy as np
import ROOT

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi
print ''


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(56); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y. 56:lo=y/hi=r. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();






### Two identities wander around room until the touch interact via some interaction radius.
# Let's do this again, except this time P2 seeks P1.
# Now with horizon range.
# Now with P1 evading P2.
# Now with wall detection.
# Try different levels of horizon, eg knowing something exists Southeast, but not exactly where;
#  Solid angle inversely proportional to distance apart.
#  Try to also redo direction vectors. Optimal vector away from P2 is orthogonal to both P2 and walls.
#  Use continuous vector analysis instead of discrete matrices.
# Add tiring effect.
# Add stealth, evading maneuvers (jump)
# Features on grid to attract or impassable


### Room dimensions
N = 100
height = N
width = N

### Make room array
# Room[y][x]
Room = np.zeros((height,width))

### Identities and starting position (y,x)
P1 = [(0,0)]
P2 = [(height,width)]

### Number of steps a turn. Previously a multiplier, now n steps instead of stepsize n. Why not both?
P1Steps = 5
P1Stepsize = 2
P2Steps = 8
P2Stepsize = 1

### Horizon (visible range, line-of-sight...) and interaction length
H1 = sqrt(N)
H2 = 2*sqrt(N)
L1 = 0



g1 = ROOT.TGraph()
g2 = ROOT.TGraph()
g1H = ROOT.TGraph() 
g2H = ROOT.TGraph() # When P1 is within horizon of P2

i = 0
while True:
  
  for step in range(P1Steps):

    # If outside of range 
    h = sqrt( (P1[-1][1]-P2[-1][1])**2 + (P1[-1][0]-P2[-1][0])**2 )
    if h > H1:
      # Generate random tuple for new single-tile direction (all nine available). If at wall, give diff direction.
      p1y = random.randint(-1,1)
      p1x = random.randint(-1,1)

      # If at bottom border, stay or go up
      if P1[-1][0] <= 0:
        p1y = random.randint(0,1)
      # If at top border, stay or go down
      if P1[-1][0] >= height:
        p1y = random.randint(-1,0)
      # If at left border, stay or go right
      if P1[-1][1] <= 0:
        p1x = random.randint(0,1)
      # If at right border, stay or go left
      if P1[-1][1] >= width:
        p1x = random.randint(-1,0)

      dir1 = (p1y*P1Stepsize, p1x*P1Stepsize)
      # Dot sum this with previous positon
      p1 = tuple(i1+i2 for i1,i2 in zip(P1[-1], dir1))

      # Append new position to array and graph
      P1.append(p1)
      g1.SetPoint(g1.GetN(), p1[1], p1[0])

    # Else (if within range) run away
    else:
      '''
      if P1[-1][0]-P2[-1][0] >= 0: p1y = 1
      if P1[-1][0]-P2[-1][0] <= 0: p1y = -1
      if P1[-1][1]-P2[-1][1] >= 0: p1x = 1
      if P1[-1][1]-P2[-1][1] <= 0: p1x = -1
      '''
      ### Cast direction values. Then check if was actually at wall (identical to above). If so, recast direction also away from P2.
      # If above, continue up
      if P1[-1][0]-P2[-1][0] > 0: p1y = 1
      # If at same horizonal, go up, stay, or down
      if P1[-1][0]-P2[-1][0] == 0: p1y = random.choice([-1,1])
      # If below, continue down
      if P1[-1][0]-P2[-1][0] < 0: p1y = -1
      # If to the right, continue right
      if P1[-1][1]-P2[-1][1] > 0: p1x = 1
      # If on same vertical, go left, stay, or right
      if P1[-1][1]-P2[-1][1] == 0: p1x = random.choice([-1,1])
      # If to the left, continue left
      if P1[-1][1]-P2[-1][1] < 0: p1x = -1

      # If at bottom border, stay or go up, and...
      if P1[-1][0] <= 0:
        p1y = random.randint(0,1)
        # If to right, continue right
        if P1[-1][1]-P2[-1][1] > 0: p1x = 1
        # If same verticle, choose left or right
        if P1[-1][1]-P2[-1][1] == 0: p1x = random.choice([-1,1])
        # If to left, continue left 
        if P1[-1][1]-P2[-1][1] < 0: p1x = -1
      # If at top border, stay or go down, and... etc.
      if P1[-1][0] >= height:
        p1y = random.randint(-1,0)
        if P1[-1][1]-P2[-1][1] > 0: p1x = 1
        if P1[-1][1]-P2[-1][1] == 0: p1x = random.choice([-1,1])
        if P1[-1][1]-P2[-1][1] < 0: p1x = -1
      if P1[-1][1] <= 0:
        if P1[-1][0]-P2[-1][0] > 0: p1y = 1
        if P1[-1][0]-P2[-1][0] == 0: p1y = random.choice([-1,1])
        if P1[-1][0]-P2[-1][0] < 0: p1y = -1
        p1x = random.randint(0,1)
      if P1[-1][1] >= width:
        if P1[-1][0]-P2[-1][0] > 0: p1y = 1
        if P1[-1][0]-P2[-1][0] == 0: p1y = random.choice([-1,1])
        if P1[-1][0]-P2[-1][0] < 0: p1y = -1
        p1x = random.randint(-1,0)

      dir1 = (p1y*P1Stepsize, p1x*P1Stepsize)
      p1 = tuple(i1+i2 for i1,i2 in zip(P1[-1], dir1))

      P1.append(p1)
      g1H.SetPoint(g1H.GetN(), p1[1], p1[0])





  ### Repeat for P2, but now P2 seeks P1 only after it is within visible range.
  for step in range(P2Steps):
    h = sqrt( (p1[1]-P2[-1][1])**2 + (p1[0]-P2[-1][0])**2 )
    if h > H2:
      p2y = random.randint(-1,1)
      p2x = random.randint(-1,1)

      if P2[-1][0] <= 0:
        p2y = random.randint(0,1)
      if P2[-1][0] >= height:
        p2y = random.randint(-1,0)
      if P2[-1][1] <= 0:
        p2x = random.randint(0,1)
      if P2[-1][1] >= width:
        p2x = random.randint(-1,0)

      dir2 = (p2y*P2Stepsize, p2x*P2Stepsize)
      p2 = tuple(i1+i2 for i1,i2 in zip(P2[-1], dir2))

      P2.append(p2)
      g2.SetPoint(g2.GetN(), p2[1], p2[0])

    else:
      # If P1 above, go up
      if p1[0]-P2[-1][0] > 0:  p2y = 1 
      # If at same hortizontal, stay
      if p1[0]-P2[-1][0] == 0: p2y = 0
      # If P1 below, go down
      if p1[0]-P2[-1][0] < 0:  p2y = -1
      # If P1 to right, go right
      if p1[1]-P2[-1][1] > 0:  p2x = 1   
      # If at same vertical, stay
      if p1[1]-P2[-1][1] == 0: p2x = 0
      # If P1 to left, go left
      if p1[1]-P2[-1][1] < 0:  p2x = -1

      dir2 = (p2y*P2Stepsize, p2x*P2Stepsize)
      p2 = tuple(i1+i2 for i1,i2 in zip(P2[-1], dir2))

      P2.append(p2)
      g2H.SetPoint(g2H.GetN(), p2[1], p2[0])
 

  # Break if P1 and P2 are within interaction range.
  if (tuple(abs(i1-i2) for i1,i2 in zip(p1, p2))[0] <= L1) and (tuple(abs(i1-i2) for i1,i2 in zip(p1, p2))[1] <= L1):
    break

  i += 1
  if i == 10000:
    break

print 'Steps:', len(P1)





### Now reconstruct position.
lineWidth = 2
# From https://root.cern.ch/doc/master/classTStyle.html#a94c9ae34f921c1efaa09348c2d16d60a
ROOT.gStyle.SetLineStyleString(5, '4 16');
lineStyle = 5

markerSize = 2
p1markerStyle = ROOT.kOpenCircle
p2markerStyle = ROOT.kOpenTriangleUp

p1Color = ROOT.kOrange
p1HColor = ROOT.kRed
p2Color = ROOT.kViolet
p2HColor = ROOT.kBlue

c = ROOT.TCanvas('c', 'c', 720*width/height, 720)

g1.SetTitle('With Interaction Length of '+str(L1)+' and Horizon of '+str(round(H2,1))+', Interaction at ~('+str(p1[1])+','+str(p1[0])+') in '+str(len(P1))+' Steps;;')
g1.GetXaxis().SetLimits(0, width)
g1.GetYaxis().SetRangeUser(0, height)
g1.SetLineColor(p1Color)
g1.SetLineWidth(lineWidth)
g1.SetLineStyle(lineStyle)
g1.Draw('pal')

g2.SetLineColor(p2Color)
g2.SetLineWidth(lineWidth)
g2.SetLineStyle(lineStyle)
g2.Draw('pl same')

g1H.SetLineColor(p1HColor)
g1H.SetLineWidth(lineWidth)
g1H.SetLineStyle(ROOT.kDashed)#lineStyle)
g1H.Draw('pl same')

g2H.SetLineColor(p2HColor)
g2H.SetLineWidth(lineWidth)
g2H.SetLineStyle(ROOT.kDashed)#lineStyle)
g2H.Draw('pl same')


gInt = ROOT.TGraph(2, np.array((float(p1[1]), float(p2[1]))), np.array((float(p1[0]), float(p2[0]))))
gInt.SetLineColor(ROOT.kBlack)
gInt.Draw('l same')

gEnd1 = ROOT.TGraph(1, np.array(float(p1[1])), np.array(float(p1[0])))
gEnd1.SetMarkerStyle(p1markerStyle)
gEnd1.SetMarkerSize(markerSize)
gEnd1.SetMarkerColor(ROOT.kBlack)
gEnd1.Draw('p same')
gEnd2 = ROOT.TGraph(1, np.array(float(p2[1])), np.array(float(p2[0])))
gEnd2.SetMarkerStyle(p2markerStyle)
gEnd2.SetMarkerSize(markerSize)
gEnd2.SetMarkerColor(ROOT.kBlack)
gEnd2.Draw('p same')

gStart1 = ROOT.TGraph(1, np.array(float(P1[0][1])), np.array(float(P1[0][0])))
gStart1.SetMarkerStyle(p1markerStyle)
gStart1.SetMarkerColor(ROOT.kBlack)
gStart1.Draw('p same')
gStart2 = ROOT.TGraph(1, np.array(float(P2[0][1])), np.array(float(P2[0][0])))
gStart2.SetMarkerStyle(p2markerStyle)
gStart2.SetMarkerColor(ROOT.kBlack)
gStart2.Draw('p same')

c.SaveAs('HorizonSeekEvadeSearch2.png')
