import math
import random
import numpy as np
import ROOT

# :%s/foo/bar/gc  Change each 'foo' to 'bar', but ask for confirmation first.

# Redefine quit to something shorter
def ex():
  quit()
# Redefine np.sqrt() to sqrt()
def sqrt(x):
  return math.sqrt(x)
# Redefine pi to something shorter
pi = math.pi
print ''


#///// Set global style /////
ROOT.gROOT.SetStyle('Plain');
ROOT.gStyle.SetPalette(56); # 51:blue. 52:lo=k/hi=w. 53:lo=r/hi=y. 56:lo=y/hi=r. 54:lo=b/hi=y. 55:lo=b/hi=r, pale rainbow. none:rainbow 
# Palettes, see https://root.cern.ch/doc/master/classTColor.html#C05. Or for ROOT 5.34: https://people.nscl.msu.edu/~noji/colormap
#// Boxes
ROOT.gStyle.SetLegendFont(132);
ROOT.gStyle.SetStatFont(132);
#// Histogram titles
ROOT.gStyle.SetTitleFont(132, 'h');
ROOT.gStyle.SetTitleSize(0.05, 'h'); #0.06
ROOT.gStyle.SetTitleBorderSize(0);
#// Axis titles
ROOT.gStyle.SetTitleFont(132, 'xyz');
ROOT.gStyle.SetTitleSize(0.04, 'xyz'); #0.04
ROOT.gStyle.SetTitleOffset(1.25, 'x'); #1.15
ROOT.gStyle.SetTitleOffset(1, 'y'); #0.7
#// Axis labels
ROOT.gStyle.SetLabelFont(132, 'xyz');
ROOT.gStyle.SetLabelSize(0.04, 'xyz');
#// Text options
ROOT.gStyle.SetTitleFont(132, 't');
ROOT.gStyle.SetTextFont(132);
ROOT.gStyle.SetTextSize(0.025)
ROOT.gStyle.SetPaintTextFormat("3.2g");
#// Legend text size
#ROOT.gStyle.SetLegendTextSize(0.1)
#// Border options
ROOT.gStyle.SetCanvasBorderSize(0);
ROOT.gStyle.SetFrameBorderSize(0);
ROOT.gStyle.SetLegendBorderSize(0);  #0,1
ROOT.gStyle.SetStatBorderSize(0);
ROOT.gStyle.SetTitleBorderSize(0);
#// Fit options
#ROOT.gStyle.SetOptFit()
ROOT.gROOT.ForceStyle();






### Two identities wander around room until the touch interact via some interaction radius.




### Room dimensions
N = 50
height = N
width = N

### Make room array
# Room[y][x]
Room = np.zeros((height,width))

### Identities and starting position (y,x)
P1 = [(0,0)]
P2 = [(height,width)]

### Interaction range
L1 = 1



'''
h1 = ROOT.TH2D('h1', 'h1', width,0,width, height,0,height)
h2 = ROOT.TH2D('h2', 'h2', width,0,width, height,0,height)
'''
g1 = ROOT.TGraph()
g2 = ROOT.TGraph()


#print P1[0], P2[0]
while True:

  # Generate random tuple for new single-tile direction (all nine available). If at wall, give diff direction.
  p1y = random.randint(-1,1)
  p1x = random.randint(-1,1)

  if P1[-1][0] == 0:
    p1y = random.randint(0,1)
  if P1[-1][0] == height:
    p1y = random.randint(-1,0)
  if P1[-1][1] == 0:
    p1x = random.randint(0,1)
  if P1[-1][1] == width:
    p1x = random.randint(-1,0)

  dir1 = (p1y, p1x)
 
  # Dot sum this with previous positon
  p1 = tuple(i1+i2 for i1,i2 in zip(P1[-1], dir1))


  # Repeat for P2
  p2y = random.randint(-1,1)
  p2x = random.randint(-1,1)

  if P2[-1][0] == 0:
    p2y = random.randint(0,1)
  if P2[-1][0] == height:
    p2y = random.randint(-1,0)
  if P2[-1][1] == 0:
    p2x = random.randint(0,1)
  if P2[-1][1] == width:
    p2x = random.randint(-1,0)

  dir2 = (p2y, p2x)

  p2 = tuple(i1+i2 for i1,i2 in zip(P2[-1], dir2))


  # Append new position to array and continue on
  P1.append(p1)
  P2.append(p2)

  #h1.Fill(p1[1],p1[0])
  #h2.Fill(p2[1],p2[0])

  g1.SetPoint(g1.GetN(), p1[1], p1[0])
  g2.SetPoint(g2.GetN(), p2[1], p2[0])
 

  #print p1, p2
 

  # Break if P1 and P2 are within interaction range.
  if (tuple(abs(i1-i2) for i1,i2 in zip(p1, p2))[0] <= L1) and (tuple(abs(i1-i2) for i1,i2 in zip(p1, p2))[1] <= L1):
    break

print 'Steps:', len(P1)




### Now reconstruct position.
lineWidth = 2
# From https://root.cern.ch/doc/master/classTStyle.html#a94c9ae34f921c1efaa09348c2d16d60a
ROOT.gStyle.SetLineStyleString(5, '4 16');
lineStyle = 5 #ROOT.kDotted

markerSize = 2
p1markerStyle = ROOT.kOpenCircle
p2markerStyle = ROOT.kOpenTriangleUp

p1Color = ROOT.kOrange
p2Color = ROOT.kViolet

c = ROOT.TCanvas('c', 'c', 720*width/height, 720)
'''
h1.SetStats(0)
h1.SetTitle(';;')
h2.SetStats(0)
h2.SetTitle(';;')
'''

g1.SetTitle('With Interaction Length L_{1}='+str(L1)+', Interaction at ~('+str(p1[1])+','+str(p1[0])+') in '+str(len(P1))+' Steps;;')
#g1.GetXaxis().SetNdivisions(215)
g1.GetXaxis().SetLimits(0, width)
g1.GetYaxis().SetRangeUser(0, height)
g1.SetLineColor(p1Color)
g1.SetLineWidth(lineWidth)
g1.SetLineStyle(lineStyle)
g1.Draw('pal')

g2.SetLineColor(p2Color)
g2.SetLineWidth(lineWidth)
g2.SetLineStyle(lineStyle)
g2.Draw('pl same')

gInt = ROOT.TGraph(2, np.array((float(p1[1]), float(p2[1]))), np.array((float(p1[0]), float(p2[0]))))
gInt.SetLineColor(ROOT.kBlack)
gInt.Draw('l same')

gEnd1 = ROOT.TGraph(1, np.array(float(p1[1])), np.array(float(p1[0])))
gEnd1.SetMarkerStyle(p1markerStyle)
gEnd1.SetMarkerSize(markerSize)
gEnd1.SetMarkerColor(ROOT.kBlack)
gEnd1.Draw('p same')
gEnd2 = ROOT.TGraph(1, np.array(float(p2[1])), np.array(float(p2[0])))
gEnd2.SetMarkerStyle(p2markerStyle)
gEnd2.SetMarkerSize(markerSize)
gEnd2.SetMarkerColor(ROOT.kBlack)
gEnd2.Draw('p same')

gStart1 = ROOT.TGraph(1, np.array(float(P1[0][1])), np.array(float(P1[0][0])))
gStart1.SetMarkerStyle(p1markerStyle)
gStart1.SetMarkerColor(ROOT.kBlack)
gStart1.Draw('p same')
gStart2 = ROOT.TGraph(1, np.array(float(P2[0][1])), np.array(float(P2[0][0])))
gStart2.SetMarkerStyle(p2markerStyle)
gStart2.SetMarkerColor(ROOT.kBlack)
gStart2.Draw('p same')

c.SaveAs('RandomSearch.png')#_steps='+str(len(P1))+'.png')
